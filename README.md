# OpenStack CLI in Docker container

## Why?
Because I want to keep my local machine nice and tidy. 

The folder `config/` contains a file called `example_secrets.env`. Make a copy of the file and call it `secrets.env` and put your OpenStack login data in it.

The `data/` folder is empty by default. This folder is intended to easily share files between your Docker container and your host system. Some examples are disk images you want to opload to OpenStack, scripts you wrote to combine some of the OpenStack commands, text files containing OpenStack command output you may need locally, etc.

## Usage
1. Get the docker image
    * You can either build the Docker image yourself: `docker build -t [REPO]/[IMAGE_NAME]:[TAG] .`
    * Or pull it from Docker Hub: `docker pull m4rcu5/oscli:0.2` 
1. Run the container (interactively)
    * by using docker `docker run -it -v $(pwd)/config:/config -v $(pwd)/data:/data --env-file=$(pwd)/config/secrets.env --rm [REPO]/[IMAGE_NAME]:[TAG] /bin/bash`
    * or with docker-compose: `docker-compose -f docker-compose.yml run --rm openstack_cli`