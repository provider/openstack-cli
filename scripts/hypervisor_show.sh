#! /bin/sh
if [ -z ${HYPERVISOR_HOSTNAME+x} ]; then
	echo "HYPERVISOR_HOSTNAME not set";
	echo "add '-e HYPERVISOR_HOSTNAME=<your_hypervisor_hostname>' to your docker-compose command"
else
	/usr/local/bin/openstack hypervisor show ${HYPERVISOR_HOSTNAME} -f json;
fi